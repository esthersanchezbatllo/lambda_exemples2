/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package varis;

/**
 *
 * @author manel, esther
 */
public class Persona {
    
    Integer edat;
    String nom;

    public Persona(Integer edat, String nom) {
        this.edat = edat;
        this.nom = nom;
    }

    public Integer getEdat() {
        return edat;
    }

    public void setEdat(Integer edat) {
        this.edat = edat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Persona{" + "edat=" + edat + ", nom=" + nom + '}';
    }
    
}
