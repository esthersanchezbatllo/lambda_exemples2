/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package exempleslambda;

import varis.Persona;
import interficies.InterficieFuncionalExemple;

/**
 *
 * @author manel, esther
 */
public class Lambda2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
         
        InterficieFuncionalExemple<Persona> lambda1;
        
        lambda1 = (x) -> x.getEdat() > 17;
        
        Persona p1 = new Persona(17, "Pepe");
        Persona p2 = new Persona(23, "Juan");
        Persona p3 = new Persona(15, "MAria");
        
        System.out.println(p3.getNom() + " es ");
        
        if (lambda1.verifica(p3))
            System.out.println(" es major d'edat");
        else
            System.out.println(" es menor d'edat");
        
         lambda1 = (x) -> x.getNom().startsWith("P");
         
          System.out.println(p1.getNom() + " comença per P? ");
         
        if (lambda1.verifica(p1))
            System.out.println(" si ");
        else
            System.out.println(" no ");
        
        
    }
    
}
