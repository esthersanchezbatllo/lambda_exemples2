/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Interface.java to edit this template
 */
package interficies;

/**
 *
 * @author manel, esther
 */
public interface InterficieFuncionalExemple<T> {
    
     public abstract boolean verifica(T valor);
    
}
